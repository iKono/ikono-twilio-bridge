# mautrix-twilio
A Matrix-Twilio relaybot bridge.

## Discussion
Matrix room: [`#twilio:maunium.net`](https://matrix.to/#/#twilio:maunium.net)

## Requirements
* Python 3.6 or higher with `pip` and `virtualenv` (Python 3.7 or higher is recommended)
* A Matrix homeserver that supports application services (e.g. [Synapse](https://github.com/matrix-org/synapse))
* Twilio account id and token
* If installing optional dependencies, see the [optional dependencies] docs.

## Development setup
0. Create a directory for the bridge.
1. Set up a virtual environment.
   1. Create with `mkvirtualenv -p /usr/bin/python3.6 ikono-infobip-bridge`
   2. Activate with `workon ikono-infobip-bridge`
2. Install dependencies with `pip install -r requirements.txt`
   * Optionally, add `-r optional-requirements.txt` to install optional dependencies. Some of the optional dependencies may need additional native packages. See the [optional dependencies] docs.
3. Copy `/mautrix_twilio/example-config.yaml` to `config.yaml`.
4. Update the config to your liking.
5. Generate the appservice registration with `python -m mautrix_twilio -g` ó con `python -m mautrix_twilio -g -c config.yaml -r registration.yaml`.
   You can use the `-c` and `-r` flags to change the location of the config and registration files.
   They default to `config.yaml` and `registration.yaml` respectively.
6. Add the path to the registration file (`registration.yaml` by default) to your Synapse's `homeserver.yaml` under `app_service_config_files`. Restart Synapse to apply changes.
7. Create the database with `alembic upgrade head`. If you have a custom config path, use `alembic -x config=/path/to/config.yaml upgrade head`
8. Run the bridge `python -m mautrix_twilio`.


### At Upgrading with database changes
1. Pull changes from Git.
2. Update dependencies with `pip install --upgrade -r requirements.txt`.
   * If you installed optional dependencies, run the same `pip` command as when installing them, but with `--upgrade`
3. Update the database with the command in install step #7.

## Production setup
- CREATE DATABASE mautrix_twilio ENCODING 'UTF8' LC_COLLATE='C' LC_CTYPE='C' template=template0 OWNER synapse;
- Pull stable docker image
- Add docker image into existing or in new docker-compose.yaml
- Up container to auto-generate config.yaml and Update it with the right config.
- Put down and up again to auto-generate the registration.yaml file
- Generate a copy of registration file into synapse directory and Add the path to your Synapse's `homeserver.yaml` under ` app_service_config_files`. Restart Synapse to apply changes.
- Finally up the container to run the app.
- If you change the appservice section in config.yaml you must to regenerate the registration file.
- If you change the config.yaml file you must always restart the twilio container.

## Optional production setup
1. Change the max_upload_size in synapse server to 16MB o more.

## Templates tips
- For bold text surround the text with one asterisk `*`
- For italic text surround the text with underscore `_`
- For strikethrough text surround the text with `~`
- For text with line breaks add `\n`

