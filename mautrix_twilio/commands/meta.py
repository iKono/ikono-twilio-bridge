# Copyright (c) 2020 Tulir Asokan
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
from typing import Dict, List

from mautrix.types import (EventID, TextMessageEventContent, MessageType)
from mautrix.bridge.commands.handler import (
    HelpSection, HelpCacheKey, command_handler,
    CommandEvent, command_handlers, SECTION_GENERAL)
from mautrix.util.simple_template import SimpleTemplate
from .. import portal as po
import json


@command_handler(
    help_section=SECTION_GENERAL,
    help_args='<international phone number>',
    help_text="Open a private chat with the given phone number.")
async def pm(evt: CommandEvent) -> EventID:

    if evt.is_portal:
        return await evt.reply("You must use this command in management room.")

    if len(evt.args) == 0:
        return await evt.reply("**Usage:** `$cmdprefix+sp pm <international phone number>`")

    phone_number = "".join(evt.args).translate({ord(c): None for c in "+()- "})
    try:
        int(phone_number)
    except ValueError:
        return await evt.reply("Invalid phone number.")

    simple_template_obj = SimpleTemplate(f"whatsapp:+{phone_number}", "number", type=int)
    formated_number = simple_template_obj.format(phone_number)
    portal = po.Portal.get_by_twid(twid=formated_number)

    if portal.mxid:
        return await evt.reply(
            f"You already have a private chat portal with that user at "
            f"<a href=\"https://matrix.to/#/{portal.mxid}\">{portal.mxid}</a>",
            allow_html=True,
            render_markdown=False
        )

    try:
        await portal.create_matrix_room()
    except Exception as err:
        return await evt.reply(f"Failed to create portal room: {err}")

    return await evt.reply("Created portal room and invited you to it.")


@command_handler(
    help_section=SECTION_GENERAL,
    help_args='{"room_id": "", "template_message": ""}',
    help_text="Send a Twilio template")
async def template(evt: CommandEvent) -> EventID:

    if evt.is_portal:
        return await evt.reply("You must use this command in management room.")

    prefix_length = len(f"{evt.command_prefix} {evt.command}")
    incoming_params = (evt.content.body[prefix_length:]).strip()
    incoming_params = json.loads(incoming_params)

    room_id = incoming_params.get("room_id")
    template_message = incoming_params.get("template_message")

    if not room_id:
        return await evt.reply("You must specify a room ID.")

    if not template_message:
        return await evt.reply("You must specify a template.")

    msg = TextMessageEventContent(body=template_message, msgtype=MessageType.TEXT)
    msg.trim_reply_fallback()

    portal = po.Portal.get_by_mxid(room_id)
    if not portal:
        return await evt.reply(f"Failed to get room {room_id}")

    await portal.handle_matrix_message(
        sender=evt.sender, message=msg, event_id=evt.event_id, is_twilio_template=True)
    return await portal.az.intent.send_message(portal.mxid, msg)  # only to be visible to the agent


@command_handler(
    help_section=SECTION_GENERAL,
    help_args='''{"room_id": "", "latitude": "", "longitude": "",
                "locationName": "", "address": ""}''',
    help_text="Send a location")
async def location(evt: CommandEvent) -> EventID:

    if evt.is_portal:
        return await evt.reply("You must use this command in management room.")

    prefix_length = len(f"{evt.command_prefix} {evt.command}")
    incoming_params = (evt.content.body[prefix_length:]).strip()
    incoming_params = json.loads(incoming_params)

    room_id = incoming_params.get("room_id")
    latitude = incoming_params.get("latitude")
    longitude = incoming_params.get("longitude")
    locationName = incoming_params.get("locationName")
    address = incoming_params.get("address")

    if not room_id:
        return await evt.reply("You must specify a room ID.")

    if not longitude or not latitude or not locationName:
        return await evt.reply("You must specify longitude, latitude and location name.")

    portal = po.Portal.get_by_mxid(room_id)
    if not portal:
        return await evt.reply(f"Failed to get room {room_id}")

    additional_data = {
        "latitude": latitude,
        "longitude": longitude,
        "address": address,
    }

    client_msg = TextMessageEventContent(body=locationName, msgtype=MessageType.LOCATION)
    client_msg.trim_reply_fallback()

    await portal.handle_matrix_message(
        sender=evt.sender, message=client_msg,
        event_id=evt.event_id, additional_data=additional_data)
    text = ""
    if locationName:
        text += f"{locationName}\n"
    if address:
        text += f"{address}\n"

    location = evt.config['bridge.google_maps_url'].replace(
        "{latitude}", latitude).replace("{longitude}", longitude)

    text += location
    agent_msg = TextMessageEventContent(body=text, msgtype=MessageType.TEXT)
    agent_msg.trim_reply_fallback()

    return await portal.az.intent.send_message(portal.mxid, agent_msg)  # only visible to the agent


@command_handler()
async def unknown_command(evt: CommandEvent) -> EventID:
    return await evt.reply("Unknown command. Try `$cmdprefix+sp help` for help.")


help_cache: Dict[HelpCacheKey, str] = {}


async def _get_help_text(evt: CommandEvent) -> str:
    cache_key = await evt.get_help_key()
    if cache_key not in help_cache:
        help_sections: Dict[HelpSection, List[str]] = {}
        for handler in command_handlers.values():
            if handler.has_help and handler.has_permission(cache_key):
                help_sections.setdefault(handler.help_section, [])
                help_sections[handler.help_section].append(handler.help + "  ")
        help_sorted = sorted(help_sections.items(), key=lambda item: item[0].order)
        helps = ["#### {}\n{}\n".format(key.name, "\n".join(value)) for key, value in help_sorted]
        help_cache[cache_key] = "\n".join(helps)
    return help_cache[cache_key]


def _get_management_status(evt: CommandEvent) -> str:
    if evt.is_management:
        return "This is a management room: prefixing commands with `$cmdprefix` is not required."
    elif evt.is_portal:
        return ("**This is a portal room**: you must always prefix commands with `$cmdprefix`.\n"
                "Management commands will not be bridged.")
    return "**This is not a management room**: you must prefix commands with `$cmdprefix`."


@command_handler(name="help",
                 help_section=SECTION_GENERAL,
                 help_text="Show this help message.")
async def help_cmd(evt: CommandEvent) -> EventID:
    return await evt.reply(_get_management_status(evt) + "\n" + await _get_help_text(evt))
